tool
extends Spatial

export(SpatialMaterial) var land_mat:SpatialMaterial
export(SpatialMaterial) var river_mat:SpatialMaterial

export(NodePath) var cam_nodepath:NodePath

export(SpatialMaterial) var primary1_mat:SpatialMaterial
export(SpatialMaterial) var primary2_mat:SpatialMaterial
export(SpatialMaterial) var primary3_mat:SpatialMaterial
export(SpatialMaterial) var primary4_mat:SpatialMaterial
export(SpatialMaterial) var primary5_mat:SpatialMaterial
export(SpatialMaterial) var primary6_mat:SpatialMaterial

export(Resource) var scene_palette setget _on_set_palette, _on_get_palette

var _scene_palette:ScenePaletteSettings
var _cam:Camera

func _on_get_palette():
	return _scene_palette

func _on_set_palette(v) -> void:
	_scene_palette = v as ScenePaletteSettings
	
	_assign(is_inside_tree())


func _assign(extern_ready:bool = false):
	if _scene_palette:
		land_mat.albedo_color = _scene_palette.land_color
		river_mat.albedo_color = _scene_palette.water_color
		
		primary1_mat.albedo_color = _scene_palette.primary1_color
		primary2_mat.albedo_color = _scene_palette.primary2_color
		primary3_mat.albedo_color = _scene_palette.primary3_color
		primary4_mat.albedo_color = _scene_palette.primary4_color
		primary5_mat.albedo_color = _scene_palette.primary5_color
		primary6_mat.albedo_color = _scene_palette.primary6_color

		if extern_ready:
			_cam = get_node(cam_nodepath)
			if _cam:
				_cam.environment.background_color = _scene_palette.sky_color

func _ready():
	_assign(true)
