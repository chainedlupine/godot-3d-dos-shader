extends Camera

export(NodePath) var following

var _following

func _ready():
	_following = get_node(following)
	call_deferred("_sync")
	
func _sync():
	# go ahead and sync us up
	global_transform = Transform(_following.global_transform.basis.rotated(Vector3(0, 1, 0), PI), _following.global_transform.origin)
	
func _process(delta):
	
	var rot = global_transform.basis.get_rotation_quat()
	# need to rotate camera around to face right way
	var target_rot = _following.global_transform.basis.rotated(Vector3(0, 1, 0), PI).get_rotation_quat()
	# sloppy but works
	var halfway_rot = rot.slerp(target_rot, 0.5 * delta)
	
	global_transform = Transform(Basis(halfway_rot), _following.global_transform.origin)
