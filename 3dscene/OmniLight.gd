extends OmniLight

export(float) var strength := 5.0
export(float) var speed := 2.0

var _time:float = 0

func _ready():
	pass
	
	
func _process(delta:float) -> void:
	_time += delta
	light_energy = clamp(sin(_time * speed), 0, 1) * strength
