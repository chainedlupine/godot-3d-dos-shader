extends OptionButton

export(NodePath) var screen_shader_rect
export(Array, Texture) var palettes

var _palettes:Array

var _shader_rect:ColorRect

func _ready():
	_shader_rect = get_node(screen_shader_rect)
	
	_palettes = []
	for pal in palettes:
		var entry = {
			"texture": pal,
			"color_size" : pal.get_size().x
		}
		var texture_name = pal.resource_path.get_basename().get_file()
		_palettes.append(entry)
		add_item(texture_name)

func _on_OptionButton_item_selected(id):
	var texture_entry = _palettes[id]
	var mat = _shader_rect.get_material()
	mat.set_shader_param("palette_lut", texture_entry.texture)
	mat.set_shader_param("max_colors", texture_entry.color_size)
