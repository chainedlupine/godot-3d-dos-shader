extends OptionButton

export(NodePath) var scene:NodePath
export(Array, Resource) var scene_palettes

var _scene

func _ready():
	_scene = get_node(scene)
	for pal in scene_palettes:
		add_item(pal.resource_path.get_basename().get_file())

func _on_ScenePaletteOptionButton_item_selected(id):
	_scene.scene_palette = scene_palettes[id]
