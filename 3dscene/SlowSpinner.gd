extends Spatial

export(Vector3) var spin_rate = Vector3(1, 0, 0)


func _process(delta):
	var mag = spin_rate.length()
	rotate_object_local(spin_rate.normalized(), delta * mag)
