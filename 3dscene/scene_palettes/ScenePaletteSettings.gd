extends Resource
class_name ScenePaletteSettings

export(Color) var land_color
export(Color) var water_color
export(Color) var sky_color

export(Color) var primary1_color
export(Color) var primary2_color
export(Color) var primary3_color
export(Color) var primary4_color
export(Color) var primary5_color
export(Color) var primary6_color
