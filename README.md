# Godot-3D-DOS-Shader

A GLES3 shader that simulates the look of early-to-mid DOS era polygonal gaming.  Can be given any palette to map the on-screen colors, with dithering to get half tones.

![screenshot](doc/screenshot.png "Screenshot")

Some notes:

Designed for 16 and 4 color palettes, which were the primary two capabilities of the CGA/EGA cards on DOS machines.  If you want to simulate VGA, I don't recommend using this shader as per-pixel LUT lookups would be expensive.

If you are using the CGA palettes, make sure to change the scene color palette to the 4-color version.

Todo:

Change the lookup to use a color LUT table, should let me do the palette lookup in one texture fetch, versus the current n (where n=colors) amount.
