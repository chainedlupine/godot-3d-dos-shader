// Shader by Chainedlupine (David Grace)
shader_type canvas_item;
render_mode unshaded;

uniform sampler2D palette_lut;
uniform float gamma = 2.2;
uniform int max_colors = 16;
uniform float lum_dither_threshold = 0.7;
uniform bool strict_nearest = true;

void vertex() 
{
}

vec3 convert_linear_to_srgb(vec3 lin_color)
{
	return pow(lin_color.rgb, vec3(1.0 / gamma));
}

float ordered_dither(int x, int y, float lum)
{
	// 4x4 map from iq@shadertoy
	const float threshold[] = {
	    1./16., 9./16., 3./16., 11./16., 
	    13./16., 5./16., 15./16., 7./16., 
	    4./16., 12./16., 2./16., 10./16.,
	    16./16., 8./16., 14./16., 6./16.
	};

	return step(threshold[4 * y + x], lum);
}

// just straight linear search for nearest color in palette source
vec3 nearest_rgb(vec3 orig_color) 
{
	float dist;
	float min_dist = 2.0;
	int idx = 0;
	ivec2 l_idx;
	vec3 pal_color;
	
	for (int i = 0; i < max_colors; i++) 
	{
		l_idx = ivec2(i, 0);
		pal_color = texelFetch(palette_lut, l_idx, 0).rgb;
		dist = distance(orig_color, pal_color);
		if (dist < min_dist) {
			min_dist = dist;
			idx = i;
		}
	}
	l_idx = ivec2(idx, 0);
	return texelFetch(palette_lut, l_idx, 0).rgb;
}

void fragment() 
{
	// sample dither map halfway, as it generates less aggressive dithering (use 4 for full samples)
	int x = int(FRAGCOORD.x) % 4;
	int y = int(FRAGCOORD.y) % 4;
	
	vec4 screen_tex = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0);
	
	// use relative luminance coeff to get our lumiance value for this pixel
	float lum = dot(vec3(0.2126, 0.7152, 0.0722), screen_tex.rgb);
	
	// now dither it
	if (lum < lum_dither_threshold)
		lum = ordered_dither(x, y, lum);
	else if (strict_nearest)
		lum = 1.0;
	
	// add in our palette color, plus any vertex colors
	COLOR.rgb = vec3(lum) * nearest_rgb(screen_tex.rgb);// * COLOR.rgb);

}

void light() {
}
